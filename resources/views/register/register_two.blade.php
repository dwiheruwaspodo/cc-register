@extends('layout.template')

@section('content')	
	<div class="card">
	  	<div class="card-header">
	    Register #2
	  	</div>
		<div class="card-body">
			@include('layout.alert')
		    <form action="{{ url('register-step2') }}" method="POST">
		    	<div class="form-group">
		    	    <label for="exampleInputEmail1">Address</label>
		    	    <input type="text" class="form-control" placeholder="Address" name="address[]" required>
		    	</div>
		    	<div class="target"></div>
		    	<button type="button" class="btn btn-sm btn-success" id="add_address">add</button>
				
				<div class="form-group">
				    <label for="exampleInputEmail1">Membership</label>
				    <select class="form-control" placeholder="CC number" name="membership" required>
				    	<option value="Silver"> Silver </option>
				    	<option value="Gold"> Gold </option>
				    	<option value="Platinum"> Platinum </option>
				    	<option value="Black"> Black </option>
				    	<option value="VIP"> VIP </option>
				    	<option value="VVIP"> VVIP </option>
				    </select>
				</div>
				<div class="form-group">
				    <label for="exampleInputEmail1">Credit Card Number</label>
				    <input type="text" class="form-control" placeholder="CC number" name="cc_number" required>
				</div>
				<div class="form-group">
				    <label for="exampleInputEmail1">Credit Card Type</label>
				    <input type="text" class="form-control" placeholder="CC type" name="cc_type" required>
				</div>
				<div class="form-group">
				    <label for="exampleInputEmail1">Credit Card Exp.Date (month)</label>
			        <select class="form-control" name="cc_exp_date_date" required>
			        	@for($x=1; $x <= 12; $x++)
			        		<option value="{{ sprintf('%02s', $x) }}"> {{ sprintf('%02s', $x) }}</option>
			        	@endfor
			        </select>
				</div>
				<div class="form-group">
				    <label for="exampleInputEmail1">Credit Card Exp.Date (year)</label>
			        <select class="form-control" name="cc_exp_date_year" required>
			        	@for($x=2018; $x <= 2030; $x++)
			        		<option value="{{ $x }}"> {{ $x }} </option>
			        	@endfor
			        </select>
				</div>
				
				<input type="hidden" name="form1" value="{{ $register }}">
		    	<hr/>

		    	{{ csrf_field() }}

		    	<button type="submit" class="btn btn-primary">Continue</button>
	    	</form>
	  	</div>
	</div>
@endsection

@section('script')	
<script type="text/javascript">
	$(document).ready(function() {
		$('#add_address').click(function() {
			var newPlace = '<div class="form-group"> \
		    	    <label for="exampleInputEmail1">Address</label> \
		    	    <input type="text" class="form-control" placeholder="Address" name="address[]" required> \
		    	    <button type="button" class="btn btn-sm btn-danger removeItem">delete</button> \
		    	</div>';

		    $('.target').append(newPlace).fadeIn(3000);
		})

		$("body").on('click', ".removeItem", function() {
		  	var mbok = $(this).parent();
		  	mbok.remove();
		});
	});
</script>
@endsection