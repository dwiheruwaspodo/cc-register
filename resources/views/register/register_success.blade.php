@extends('layout.template')

@section('content')	
	<div class="card">
	  	<div class="card-header">
	    Register #3
	  	</div>
		<div class="card-body">			
			<div class="jumbotron">
			  	<h1 class="display-4">@include('layout.alert')</h1>
			  	<p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
			  	<hr class="my-4">
			  	<p>LOGIN : </p>
			  	<a class="btn btn-primary btn-lg" href="{{ url('login') }}" role="button">Click</a>
			</div>
	  	</div>
	</div>
@endsection