@extends('layout.template')

@section('content')	
	<div class="card">
	  	<div class="card-header">
	    Register #1
	  	</div>
		<div class="card-body">
			@include('layout.alert')
		    <form action="{{ url()->current() }}" method="POST">
		    	<div class="form-group">
		    	    <label for="exampleInputEmail1">First Name</label>
		    	    <input type="text" class="form-control" placeholder="First name" name="first_name" value="{{ old('first_name') }}">
		    	</div>
		    	<div class="form-group">
		    	    <label for="exampleInputEmail1">Last Name</label>
		    	    <input type="text" class="form-control" placeholder="Last name" required name="last_name" value="{{ old('last_name') }}">
		    	</div>
		    	<div class="form-group">
		    	    <label for="exampleInputEmail1">Email</label>
		    	    <input type="text" class="form-control" placeholder="Enter email" required name="email" value="{{ old('email') }}">
		    	</div>
		    	<div class="form-group">
		    	    <label for="exampleInputPassword1">Password</label>
		    	    <input type="password" class="form-control" placeholder="Password" required name="password">
		    	</div>
		    	<div class="form-group">
		    	    <label for="exampleInputPassword1">Confirm Password</label>
		    	    <input type="password" class="form-control" placeholder="Confirm Password" required name="password_confirmation">
		    	</div>
		    	
		    	<hr/>

		    	{{ csrf_field() }}

		    	<button type="submit" class="btn btn-primary">Continue</button>
	    	</form>
	  	</div>
	</div>
@endsection