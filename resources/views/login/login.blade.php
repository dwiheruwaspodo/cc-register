@extends('layout.template')

@section('content')	
	<div class="card">
	  	<div class="card-header">
	    Login
	  	</div>
		<div class="card-body">
	    	@include('layout.alert')
		    <form action="{{ url()->current() }}" method="POST">
		    	 <div class="form-group">
		    	    <label for="exampleInputEmail1">Email address</label>
		    	    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email">
		    	</div>
		    	<div class="form-group">
		    	    <label for="exampleInputPassword1">Password</label>
		    	    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
		    	</div>
		    	{{ csrf_field() }}
		    	<button type="submit" class="btn btn-primary">Submit</button>
	    	</form>
	    	<small class="form-text text-muted">Nggak punya account? Register <a href="{{ url('register') }}"> disini. </a></small>
	  	</div>
	</div>
@endsection