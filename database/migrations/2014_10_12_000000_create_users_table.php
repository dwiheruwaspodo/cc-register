<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 60)->nullable();
            $table->string('last_name', 60);
            $table->string('password');
            $table->string('email', 60)->unique();
            $table->enum('membership', ['Silver', 'Gold', 'Platinum', 'Black', 'VIP', 'VVIP',])->nullable();
            $table->string('cc_number', 100);
            $table->string('cc_type', 100);
            $table->date('cc_exp_date');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
