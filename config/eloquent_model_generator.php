<?php

return [
    'model_defaults' => [
	    'namespace'       => 'App',
        'base_class_name' => \Illuminate\Database\Eloquent\Model::class,
    ],

    'db_types' => [
        'enum' => 'string',
    ],
];