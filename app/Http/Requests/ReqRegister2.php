<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;

class ReqRegister2 extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cc_number'        => ['required', new CardNumber],
            'cc_exp_date_year' => ['required', new CardExpirationYear($this->get('cc_exp_date_year'))],
            'cc_exp_date_date' => ['required', new CardExpirationMonth($this->get('cc_exp_date_date'))],
        ];
    }
}
