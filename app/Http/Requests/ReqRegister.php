<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReqRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => '',
            'last_name'  => 'sometimes|required|max:50',
            'email'      => 'sometimes|unique:users,email|regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
            'password'   => 'sometimes|confirmed|required|min:3|max:50'
        ];
    }
}
