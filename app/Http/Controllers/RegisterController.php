<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;
use Session;

use App\Http\Requests\ReqRegister;
use App\Http\Requests\ReqRegister2;
use App\User;
use App\Address;
use Hash;
use DB;

class RegisterController extends Controller
{
    function __construct() 
    {
    	date_default_timezone_set('Asia/Jakarta');
    }

    function register(ReqRegister $request)
    {
    	
    	$post = $request->except('_token');

    	if ($post) {
    		$data['register'] = json_encode($post);

    		return view('register.register_two', $data);		
    	}

    	return view('register.register');
    }

    function registerStep2(ReqRegister2 $request)
    {
    	$post = $request->except('_token');

    	if (empty($post)) {
    		return redirect('register');
    	}

    	$post['form1'] = json_decode($post['form1'], true);

    	$dataSave = [
			'first_name'  => $post['form1']['first_name'],
			'last_name'   => $post['form1']['last_name'],
			'password'    => Hash::make($post['form1']['password']),
			'email'       => $post['form1']['email'],
			'membership'  => $post['membership'],
			'cc_number'   => $post['cc_number'],
			'cc_type'     => $post['cc_type'],
			'cc_exp_date' => date('Y-m-d', strtotime($post['cc_exp_date_year'].'-'.$post['cc_exp_date_date'].'-01')),
    	];

    	DB::beginTransaction();

    	if ($saveuser = User::create($dataSave)) {
    		$address = [];

    		foreach ($post['address'] as $key => $value) {
    			$temp = [
					'address'    => $value,
					'id_user'    => $saveuser->id,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
    			];

    			array_push($address, $temp);
    		}

    		// SAVE REGISTER
    		$saveuser = Address::insert($address);   
    	}

    	return parent::redirect($saveuser, 'Yay! You have been registered.', 'register-success');
    	// return view('register.register_two');
    }

    function registerSuccess()
    {
    	return view('register.register_success');
    }
}
