<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;
use Session;
use Auth;
use App\User;

class LoginController extends Controller
{
    function __construct() 
    {
    	date_default_timezone_set('Asia/Jakarta');
    }

    function login(Request $request)
    {
    	$post = $request->except('_token');

    	if ($post) {

    		$login = Auth::attempt($post);

    		if ($login) {
    			print_r(Auth::user());
    			exit();
    		}

            return parent::redirect($login, "Yay! Login");
    	}

    	return view('login.login');
    }
}
