<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function redirect($save, $messagesSuccess, $next=null) {
        if ($save) {
            DB::commit();

            if (is_null($next)) {
                return back()->with('success', [$messagesSuccess]);
            }
            else {
                return redirect($next)->with('success', [$messagesSuccess]);
            }
        }
        else {
        	DB::rollback();
            $e = ['e' => 'Something went wrong, gaes.'];
            return back()->witherrors($e)->withInput();
        }
    }
}
