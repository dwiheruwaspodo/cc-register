<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_address_user
 * @property int $id_user
 * @property string $address
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Address extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user_address';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_address_user';

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'address', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Http\User', 'id_user');
    }
}
