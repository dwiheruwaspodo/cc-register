<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $email
 * @property string $membership
 * @property string $cc_number
 * @property string $cc_type
 * @property string $cc_exp_date
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property UserAddress[] $userAddresses
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'password', 'email', 'membership', 'cc_number', 'cc_type', 'cc_exp_date', 'remember_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userAddresses()
    {
        return $this->hasMany('App\Http\UserAddress', 'id_user');
    }
}
